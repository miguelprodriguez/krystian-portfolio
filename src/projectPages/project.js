import React from 'react';
import { Jumbotron, Container, Row } from 'react-bootstrap';
import Footer from '../components/Footer';
import ProjectContainer from '../components/ProjectContainer';
import styles from './project.module.css';
import data from '../data/data';

export default function Project({match}) {
	// React router pass two props to all the component e.g. match & location
	const endpoint = match.params.id
	const specificProject = data.find((x, index) => index === endpoint - 1)

	return(
		<React.Fragment>
			<Jumbotron className={styles.centerContainer}>
				<Container className="pt-5">
					<Row>
						<ProjectContainer image={specificProject?.image} title={specificProject?.title} />
					</Row>
				</Container>
			</Jumbotron>

			<Footer />
		</React.Fragment>
		)
}