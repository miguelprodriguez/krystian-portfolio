import Card1 from '../assets/images/card1.jpg';
import Card2 from '../assets/images/card2.jpg';
import Card3 from '../assets/images/card3.jpg';
import Card4 from '../assets/images/card4.jpg';

import Card5 from '../assets/images/card5.jpg';
import Card6 from '../assets/images/card6.jpg';
import Card7 from '../assets/images/card7.jpg';
import Card8 from '../assets/images/card8.jpg';

let projects = [
	{
        id: 1, 
		image: Card1, 
        title: 'Waves'
	},
	{
        id: 2, 
		image: Card2, 
        title: 'Sebulok'
	},
	{
        id: 3, 
		image: Card3, 
        title: 'Pantene'
	},
	{
        id: 4,
		image: Card4, 
        title: 'Why U Cry?'
	},
	{
        id: 5,
		image: Card5, 
        title: 'Vigan'
	},
	{
        id: 6, 
		image: Card6, 
        title: 'Japan!Japan!'
	},
	{
        id: 7,
		image: Card7, 
        title: 'Tuwing Umuulan at Umuulan'
	},
	{
        id: 8,
		image: Card8, 
        title: 'Vape Lods'
	}
]

export default projects