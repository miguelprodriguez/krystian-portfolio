import React from 'react';
import Portfolio from '../components/Portfolio';
import Footer from '../components/Footer';

export default function Homepage() {
	return(
		<React.Fragment>
			<Portfolio />
			<Footer />
		</React.Fragment>
		)
}