import React from 'react';
import Contact from '../components/Contact';
import Footer from '../components/Footer';

export default function ContactPage() {
	return(
		<React.Fragment>
			<Contact  />
			<Footer  />
		</React.Fragment>
		)
}