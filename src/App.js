import './App.css';
import './NavBar.css';
import './home.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import NavBar from './components/NavBar';
import ScrollToTop from './components/ScrollToTop';

import Home from './pages/homepage';
import About from './pages/aboutPage';
import Contact from './pages/contactPage';
import Project from './projectPages/project';

import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';

function App() {
  return (
    <div className="App">
    <Router>
    <NavBar />
    <ScrollToTop />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/about" component={About} />
        <Route path="/contact" component={Contact} />
        <Route path="/project/:id" component={Project} />
      </Switch>
    </Router>
    </div>
  );
}

export default App;
