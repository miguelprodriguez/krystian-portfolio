import React from 'react';
import { Container, Row } from 'react-bootstrap';
import ImageContainer from './ImageContainer';
import data from '../data/data';

export default function Home() {

	const boxes = data.map((data, index) => {
		return (
			<ImageContainer
				images={data.image}
				titles={data.title}
				id={data.id}
				key={index}
			/>
		)
	})

	return (
		<React.Fragment>
			<Container className="projects" fluid>
				<Row>
					{boxes}
				</Row>
			</Container>
		</React.Fragment>
	)
}