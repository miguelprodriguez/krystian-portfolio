import React from 'react';
import { Jumbotron } from 'react-bootstrap';
import styles from './Footer.module.css'
import { useLocation } from 'react-router-dom';

export default function Footer() {
	const atHome = useLocation().pathname === '/' 
	
	return(
		<Jumbotron className={`${styles.footer} ${atHome ? '' : `fixed-bottom`} text-center`}>
			<p>Krystian Millora 2021 &copy;</p>
		</Jumbotron>
		)
}