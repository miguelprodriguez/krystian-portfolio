import React from 'react';
import { Nav, Navbar } from 'react-bootstrap';
import { Link } from "react-router-dom";

import brandImage from '../assets/images/logo.png';

export default function NavBar() {
	return (
		<Navbar collapseOnSelect bg="light" expand="lg" className="whiteImage" >
			<Navbar.Brand as={Link} to="/" className="img-responsive">
				<img src={brandImage} className="brandImage" alt="" />
			</Navbar.Brand>

			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Nav.Link eventKey="1" as={Link} className="black-text" to="/">HOME</Nav.Link>
					<Nav.Link eventKey="1" as={Link} className="black-text" to="/about">ABOUT</Nav.Link>
					<Nav.Link eventKey="1" as={Link} className="black-text" to="/contact">CONTACT</Nav.Link>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}