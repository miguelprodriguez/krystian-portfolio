import React from 'react';
import styles from './Contact.module.css'

export default function About() {
	return (
		<React.Fragment>
			<div className="footer text-center">
				<strong>
					<div className={`my-5 py-5 ${styles.inMiddle} text-center`}>
						<h5 className="pb-2">
							<a href="mailto:tmillorakrystianjoseph@gmail.com?subject=Portfolio Inquiry">
								millorakrystianjoseph@gmail.com
							</a>
						</h5>
						<h5>
							<a href="tel:+639175128929">+63917 512 8929</a>
						</h5>
					</div>
				</strong>
			</div>
		</React.Fragment>
	)
}