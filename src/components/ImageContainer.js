import React from 'react';
import { Link } from 'react-router-dom';
import { Col } from 'react-bootstrap';

export default function ImageContainer({ images, titles, id }) {
    return (
        <Col lg={3} className="px-0">
            <Link to={`project/${id}`}>
                <div className="containerImage">
                    <img src={images} className="wrapper img-fluid" alt="" />
                    <div className="description">
                        <div className="description-positioning">
                            <h1>{titles}</h1>
                        </div>
                    </div>
                </div>
            </Link>
        </Col>
    )
}
